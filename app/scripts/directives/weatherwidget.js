'use strict';

angular
	.module('weatherWidgetApp')
	.directive('weather', function($http) {
		return {
			templateUrl: 'scripts/templates/weatherwidget.html',
			restrict: 'A',
			link: function postLink($scope, element, attrs) {
				var numberOfDays = attrs.days || 5;
				var source = 'http://api.openweathermap.org/data/2.5/forecast/daily?id=2643743&units=metric&cnt=' + numberOfDays;
				$http
					.get(source)
					.success(function(data) {
						$scope.weatherData = data;
					});
			}
		};
	});
