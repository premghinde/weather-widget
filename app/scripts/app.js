'use strict';

angular.module('weatherWidgetApp', [
  'ngCookies',
  'ngSanitize',
  'ngRoute'
])
  .config(function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'WeatherCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
